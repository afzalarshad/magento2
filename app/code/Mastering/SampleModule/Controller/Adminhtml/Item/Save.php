<?php

namespace Mastering\SampleModule\Controller\Adminhtml\Item;

use Mastering\SampleModule\Model\ItemFactory;

class Save extends \Magento\Backend\App\Action
{
    private $itemFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        ItemFactory $itemFactory
    ) {
        $this->itemFactory = $itemFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $item = $this->itemFactory->create();
        $data = $this->getRequest()->getParams(['general']);
        $item->setData($data);
        $item->save();
        return $this->resultRedirectFactory->create()->setPath('mastering/index/index');
    }
}
