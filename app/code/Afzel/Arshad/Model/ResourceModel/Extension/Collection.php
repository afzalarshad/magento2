<?php
namespace Afzel\Arshad\Model\ResourceModel\Extension;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'afzel_arshad_extension_collection';
    protected $_eventObject = 'extension_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Afzel\Arshad\Model\Extension', 'Afzel\Arshad\Model\ResourceModel\Extension');
    }
}
