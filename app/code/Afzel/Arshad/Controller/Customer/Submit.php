<?php
namespace Afzel\Arshad\Controller\Customer;
use \Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Customer\Model\CustomerFactory;

class Submit extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;
    protected $customer;
    protected $storeManager;
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
       \Magento\Store\Model\StoreManagerInterface $storeManager,
       \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // return $this->_pageFactory->create();
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $store = $this->storeManager->getStore();
        $storeId = $store->getStoreId();
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        
        $data= $this->getRequest()->getPost();
        if($data)
        {
            $firstname = $data['firstname'];
            $lastname  = $data['lastname'];
            $email = $data['email'];
            $password = $data['password'];

        } 

        // $customer->setEmail('email' , $email); 
        // $customer->setFirstName('firstname' , $firstname);
        // $customer->setLastName('lastname' , $lastname);
        // $customer->setPassword('password',$password);

        // $customer->setData('email' , $email); 
        // $customer->setData('firstname' , $firstname);
        // $customer->setData('lastname' , $lastname);
        // $customer->setData('password',$password);
        // =================================================
        // $customer->setEmail($email); 
        // $customer->setFirstName($firstname);
        // $customer->setLastName($lastname);
        // $customer->setPassword($password);

        $customer->setData('email' , $email); 
        $customer->setData('firstname' , $firstname);
        $customer->setData('lastname' , $lastname);
        $customer->setData('password',$password);
        
        $customer->save();

        // ======================================================
        // $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // $customer   = $this->customerFactory->create();
        // $customer->setWebsiteId($websiteId);
        // $customer->setEmail("john2@example.com"); 
        // $customer->setFirstname("John");
        // $customer->setLastname("Dee");
        // $customer->setPassword("123XYZ");
        // $customer->save();
        // $customer->sendNewAccountEmail();
        // return $this->_pageFactory->create();
        // echo "Hello World!";
        // exit;
        // =========================================================
        // try{
        //     $customer = $this->getRequest()->getPost();
        //     $data = $this->customerFactory->create();

        //     if($customer)
        //     {
        //         $name = $customer['name'];
        //         $email =$customer['email'];
        //     }
        //     $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        //     $data->setWebsiteId($websiteId);
        //     $data->setEmail($email); 
        //     $data->setFirstname($name);
        //     $data->setLastname($name);
        //     $data->setPassword("123XYZ");
        //     $data->save();
            
        //     $this->messageManager->addSuccessMessage(__("Data has been save Successfully "));
        // }catch (\Exception $e)
        //     {
        //         $this->messageManager->addErrorMessage($e, __("Can't save the data , please try again"));
        //     }=
        //=============================================================================================================================
        //  return $this->_pageFactory->create();

    }
}