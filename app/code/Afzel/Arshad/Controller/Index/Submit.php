<?php
namespace Afzel\Arshad\Controller\Index;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Afzel\Arshad\Model\ExtensionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;


class Submit extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected PageFactory $resultPageFactory;
    protected ExtensionFactory $extensionFactory;
    private UrlInterface $url;
    protected $messageManager;
    // private CustomerFactory $customerFactory;
    protected Customer $customer;

    private \Magento\Store\Model\StoreManagerInterface $storeManager;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        UrlInterface $url,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Store\Model\StoreManagerInterface $storeManager,
       PageFactory $resultPageFactory,
       ExtensionFactory $extensionFactory,
       \Magento\Framework\Message\ManagerInterface $messageManager,
       \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Customer $customers
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->storeManager = $storeManager;
        $this->extensionFactory = $extensionFactory;
        $this->messageManager = $messageManager;
        $this->url = $url;
        $this->customerFactory = $customerFactory;
        $this->customer = $customers;
        return parent::__construct($context);
    }

    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        // return $this->_pageFactory->create();

        // echo "Hello World!";
        // exit;
        try {
            $data = (array)$this->getRequest()->getPost();

            if ($data) {
                $model = $this->extensionFactory->create();
                $model->setData($data)->save();
                $this->messageManager->addSuccessMessage(__("Data Saved Successfully."));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __("We can\'t submit your request, Please try again."));
        }

    //     $store = $this->storeManager->getStore();
    //     $storeId = $store->getStoreId();
    //     $websiteId = $this->storeManager->getStore()->getWebsiteId();
    //     $customer = $this->customerFactory->create();
    //    $customer->setWebsiteId($websiteId);
    //     $customer->setWebsiteId($websiteId)
    //     ->setStore($store)
    //     ->setName(['name'])
    //     ->setEmail(['email']);
    //     $customer->save();

    
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->url->getUrl('pyaray/index/showdata'));
        return $resultRedirect;


    }
}
