<?php
namespace Afzel\Arshad\Block;
use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Template\Context;
use Afzel\Arshad\Model\ExtensionFactory;
class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    private $extensionFactory;

    public function __construct(
        ExtensionFactory $extensionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->extensionFactory = $extensionFactory;

    }
    public function getFormAction()
    {
        return $this->getUrl('pyaray/index/submit', ['_secure' => true]);
    }
    public function getAllData()
    {
        $id = $this->getRequest()->getParam("id");
        $model = $this->extensionFactory->create();
        return $model->load($id);
    }
}
