<?php
namespace Pfay\Contacts\Model\ResourceModel\Conatacts;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'pfay_contacts_id';
    protected $_eventPrefix = 'pfay_contacts_conatacts_collection';
    protected $_eventObject = 'conatacts_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Pfay\Contacts\Model\Conatacts', 'Pfay\Contacts\Model\ResourceModel\Conatacts');
    }
}
