<?php
namespace Rltsquare\Tasks\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.4')<0) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('tasks'),
                'estimate_hours',
                'estimate_hours',
                [
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'   => 30,
                    'nullable' => false,
                    'comment'  => 'Estimate Time'
                ]
            );
            $setup->getConnection()->changeColumn(
                $setup->getTable('tasks'),
                'assignee_name',
                'assignee_name',
                [
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'   => 30,
                    'nullable' => false,
                    'comment'  => 'User Name'
                ]
            );
        }

        $setup->endSetup();
    }

}
