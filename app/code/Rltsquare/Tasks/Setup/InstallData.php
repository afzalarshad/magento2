<?php
namespace Rltsquare\Tasks\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    protected $postFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, 
    \Rltsquare\Tasks\Model\PostFactory $postFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->postFactory=$postFactory;

    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $data = [
            'tasks_name'=>'Install Data',
            'description'=>'Insert Data in database using InstallData',
            'estimate_hours'=>'1 week',
            'assignee_name'=>'Afzel',
            'status_boolean'=>1,
        ];
        $post = $this->postFactory->create();
        $post->addData($data)->save();
        
    }
}