<?php
namespace Rltsquare\Tasks\Model\ResourceModel\Blog;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'rltsquare_tasks_blog_collection';
    protected $_eventObject = 'blog_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Rltsquare\Tasks\Model\Blog', 'Rltsquare\Tasks\Model\ResourceModel\Blog');
    }
}
