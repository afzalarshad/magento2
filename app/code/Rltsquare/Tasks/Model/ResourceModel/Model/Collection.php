<?php
namespace Rltsquare\Tasks\Model\ResourceModel\Model;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'rltsquare_tasks_model_collection';
    protected $_eventObject = 'model_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Rltsquare\Tasks\Model\Model', 'Rltsquare\Tasks\Model\ResourceModel\Model');
    }
}
