<?php

namespace Rltsquare\Tasks\Controller\Image;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;
use Rltsquare\Tasks\Model\ModelFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\App\Action\Action;


class Save implements \Magento\Framework\App\ActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $messageManager;
    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;
    protected $urlInterface;

    /**
     * @var AdapterFactory
     */
    protected $adapterFactory;
    protected $modelFactory;


    /**
     * @var Filesystem
     */
    protected $filesystem;
    protected $redirect;

    protected  $request;
    protected $responseFactory;
    protected $url;
    protected $resultRedirect;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Rltsquare\Tasks\Model\ModelFactory $modelFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        UrlInterface $urlInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        RedirectFactory $resultRedirect
    ) {
        $this->_pageFactory = $pageFactory;
        $this->modelFactory = $modelFactory;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->messageManager = $messageManager;
        $this->urlInterface = $urlInterface;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->resultRedirect=$resultRedirect;
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // $this->_view->loadLayout();
        // $this->_view->renderLayout();
        $data = $this->request->getParams();
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            try {
                $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $imageAdapter = $this->adapterFactory->create();
                $uploaderFactory->addValidateCallback('custom_image_upload', $imageAdapter, 'validateUploadFile');
                $uploaderFactory->setAllowRenameFiles(true);
                $uploaderFactory->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('images');
                $result = $uploaderFactory->save($destinationPath);
                if (!$result) {
                    throw new LocalizedException(
                        __('File cannot be saved to path: $1', $destinationPath)
                    );
                }
                $imagePath = 'images' . $result['file'];
                $data['image'] = $imagePath;
            } catch (\Exception $e) {
            }
        }
        $model = $this->modelFactory->create();
        $model->setData($data);
        if ($model->save()) {
            $this->messageManager->addSuccessMessage(__('You saved the data.'));
        } else {
            $this->messageManager->addErrorMessage(__('Data was not saved.'));
        }

        $resultRedirect = $this->resultRedirect->create();
        $resultRedirect->setPath('rltsquare/image/index');
        return $resultRedirect;

    }
}
