<?php

namespace Rltsquare\Tasks\Controller\Image;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;
use Rltsquare\Tasks\Model\ModelFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\UrlInterface;


class Index implements \Magento\Framework\App\ActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $messageManager;
    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;
    protected $urlInterface;

    /**
     * @var AdapterFactory
     */
    protected $adapterFactory;
    protected $modelFactory;

    /**
     * @var Filesystem
     */
    protected $filesystem;
    protected $redirect;

    protected  $request;
    protected $responseFactory;
    protected $url;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Rltsquare\Tasks\Model\ModelFactory $modelFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Response\Http $redirect,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        UrlInterface $urlInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url
    ) {
        $this->_pageFactory = $pageFactory;
        $this->modelFactory = $modelFactory;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->messageManager = $messageManager;
        $this->urlInterface = $urlInterface;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // $this->view->loadLayout();
        // $this->view->renderLayout();
        return $this->_pageFactory->create();

        // $data = $this->request->getParams();
        // if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
        //     try {
        //         $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
        //         $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
        //         $imageAdapter = $this->adapterFactory->create();
        //         $uploaderFactory->addValidateCallback('custom_image_upload', $imageAdapter, 'validateUploadFile');
        //         $uploaderFactory->setAllowRenameFiles(true);
        //         $uploaderFactory->setFilesDispersion(true);
        //         $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
        //         $destinationPath = $mediaDirectory->getAbsolutePath('images');
        //         $result = $uploaderFactory->save($destinationPath);
        //         if (!$result) {
        //             throw new LocalizedException(
        //                 __('File cannot be saved to path: $1', $destinationPath)
        //             );
        //         }
        //         $imagePath = 'images' . $result['file'];
        //         $data['image'] = $imagePath;
        //     } catch (\Exception $e) {
        //     }
        // }
        // $model = $this->modelFactory->create();
        // $model->setData($data);
        // if ($model->save()) {
        //     $this->messageManager->addSuccessMessage(__('You saved the data.'));
        // } else {
        //     $this->messageManager->addErrorMessage(__('Data was not saved.'));
        // }

        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // $redirect = $objectManager->get('\Magento\Framework\App\Response\Http');
        // $redirect->setRedirect('rltsquare/image/index');
        // $redirect = $this->resultFactory->create();

        // $redirect->setPath('rltsquare/image/index');
        // return $redirect;

        // Get referer url
        // $url = $this->redirect->getRefererUrl();

        // Or get any custom url
        //$url = $this->urlInterface->getUrl('my/custom/url');

        // Create login URL
        // $login_url = $this->urlInterface
        //     ->getUrl(
        //         'rltsquare/image/index'
        //     );

        // Redirect to login URL
        // $resultRedirect = $this->resultRedirectFactory->create();
        // $resultRedirect->setUrl($login_url);
        // return $resultRedirect;
        // $url = $this->url->getUrl('rltsquare/image/index'); //custom path URL
        // return $this->getResponse()->setRedirect($url);
        
    }
}



// namespace Rltsquare\Tasks\Controller\Image;
 
//  use Magento\Framework\App\Action\Context;
//  use Rltsquare\Tasks\Model\ModelFactory;
//  use Magento\Framework\App\Filesystem\DirectoryList;
//  use Magento\MediaStorage\Model\File\UploaderFactory;
//  use Magento\Framework\Image\AdapterFactory;
//  use Magento\Framework\Filesystem;
//  use Magento\Framework\Controller\ResultFactory; 

 
//  class Index extends \Magento\Framework\App\Action\Action
//  {
//   protected $_test;
//   protected $uploaderFactory;
//   protected $adapterFactory;
//   protected $filesystem;
  
//   public function __construct(
//    Context         $context,
//    ModelFactory     $test,
//    UploaderFactory $uploaderFactory,
//    AdapterFactory  $adapterFactory,
//    Filesystem      $filesystem,
//    \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
//   )
//   {
//    $this->_test = $test;
//    $this->uploaderFactory = $uploaderFactory;
//    $this->adapterFactory = $adapterFactory;
//    $this->filesystem = $filesystem;
//    $this->resultRedirectFactory = $resultRedirectFactory;

//    parent::__construct($context);
//   }
  
//   public function execute()
//   {
//    $data = $this->getRequest()->getParams();
//    if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
//     try {
//      $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
//      $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
//      $imageAdapter = $this->adapterFactory->create();
//      $uploaderFactory->addValidateCallback('custom_image_upload', $imageAdapter, 'validateUploadFile');
//      $uploaderFactory->setAllowRenameFiles(true);
//      $uploaderFactory->setFilesDispersion(true);
//      $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
//      $destinationPath = $mediaDirectory->getAbsolutePath('images');
//      $result = $uploaderFactory->save($destinationPath);
//      if (!$result) {
//       throw new LocalizedException(
//        __('File cannot be saved to path: $1', $destinationPath)
//       );
//      }
//      $imagePath = 'images' . $result['file'];
//      $data['image'] = $imagePath;
//     } catch (\Exception $e) {
//     }
//    }
//    $test = $this->_test->create();
//    $test->setData($data);
//    if ($test->save()) {
//     $this->messageManager->addSuccessMessage(__('You saved the data.'));
//    } else {
//     $this->messageManager->addErrorMessage(__('Data was not saved.'));
//    }

//    $resultRedirect = $this->resultRedirectFactory->create();
//    $url = 'http://localhost/magento/pub/rltsquare/image/index/';
//    $resultRedirect->setUrl($url);
//    return $resultRedirect;
//    return $this->resultRedirectFactory->create()->setPath('rltsquare/image/index/', ['_current' => true]);
//    return $this->resultRedirectFactory->create()->setUrl(rm_request('rltsquare/image/index'));

//    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

//             // Your code

//             $resultRedirect->setUrl($this->_redirect->getRefererUrl());
//             return $resultRedirect;
//   }
//  }