<?php
namespace Rltsquare\Tasks\Controller\Form;
use Rltsquare\Tasks\Model\Post;
use Rltsquare\Tasks\Model\PostFactory;

class Form implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;
    protected $postFactory;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
       PostFactory $postFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->postFactory = $postFactory;
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        return $this->_pageFactory->create();
       
    }
}
