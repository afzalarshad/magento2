<?php
namespace Rltsquare\Tasks\Controller\Form;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Rltsquare\Tasks\Model\ModelFactory;
use Magento\Framework\UrlInterface;


class Submit implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;
    protected $postFactory;
    protected $data;
    protected $request;
    protected $urlInterface;

    protected $redirect;

    protected $responseFactory;
    protected $url;
    protected $resultRedirect;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
       \Rltsquare\Tasks\Model\PostFactory $postFactory,
       \Magento\Framework\App\Request\Http $request,
       \Magento\Framework\Controller\ResultFactory $resultFactory,
       \Magento\Framework\App\Response\RedirectInterface $redirect,
       \Magento\Framework\Message\ManagerInterface $messageManager,
       UrlInterface $urlInterface,
       \Magento\Framework\App\ResponseFactory $responseFactory,
       \Magento\Framework\UrlInterface $url,
       RedirectFactory $resultRedirect
    )
    {
        $this->pageFactory = $pageFactory;
        $this->postFactory = $postFactory;
        $this->request = $request;
        $this->resultRedirect=$resultRedirect;
        $this->resultFactory = $resultFactory;

    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data= (array)$this->request->getParams();
        $model=$this->postFactory->create();
        $model->setData($data);
        $model->save();

        $resultRedirect = $this->resultRedirect->create();
        $resultRedirect->setPath('rltsquare/form/form');
        return $resultRedirect;
        // return $this->pageFactory->create();

    }
}
