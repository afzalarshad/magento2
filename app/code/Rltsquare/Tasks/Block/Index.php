<?php
namespace Rltsquare\Tasks\Block;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
    public function getCompany()
    {
        return __('RLTSquare Tasks');
    }
    public function getName()
    {
        return __('Afzel Arshad');
    }
    public function getImage()
    {
        // $imagePath = $this->getViewFileUrl('images/1.jpg');
        // $image = file_get_contents($imagePath );
        //         header('content-type: image/jpeg');
        //         echo $image;
        // echo "<img src="{{view url="images/1.jpg"}}" />";
        return $this->getViewFileUrl('Rltsquare_Tasks::images/1.jpg');
    }
    public function getEducation()
    {
        return __('Bachelour In Computer Science');
    }
    public function getDOB()
    {
        return __('01/09/1996');
    }
    public function getLinkedInProfile()
    {
        return __('https://www.linkedin.com/in/afzel-arshad-91b761117/');
    }
}
