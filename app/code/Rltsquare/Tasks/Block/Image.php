<?php
namespace Rltsquare\Tasks\Block;

class Image extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Rltsquare\Tasks\Model\ModelFactory $modelFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->modelFactory = $modelFactory;

    }
    public function getImageData()
    {
        $model = $this->modelFactory->create();
		return $model->getCollection();
    }
    // public function getImage()
    // {

    //     $objectManager =\Magento\Framework\App\ObjectManager::getInstance();
    //     $helperImport = $objectManager->get('\Magento\Catalog\Helper\Image');

    //     $imageUrl = $helperImport->init($product, 'custom_image_upload')
    //         ->setImageFile($product->getSmallImage()) // image,small_image,thumbnail
    //         ->resize(380)
    //         ->getUrl();
    //     return $imageUrl;
    // }
//     function getMediaBaseUrl() {
//         /** @var \Magento\Framework\ObjectManagerInterface $om */
//         $om = \Magento\Framework\App\ObjectManager::getInstance();
//         /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
//         $storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
//         /** @var \Magento\Store\Api\Data\StoreInterface|\Magento\Store\Model\Store $currentStore */
//         $currentStore = $storeManager->getStore();
//         return $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
//     }
// public function getURls()
// {
// $objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
// $product_id = 32; //Replace with your product ID
// $productimages = array();
// $product = $objectmanager ->create('Magento\Catalog\Model\Product')->load($product_id);
// $productimages = $product->getMediaGalleryImages();
// foreach($productimages as $productimage)
// {
//  echo "<img src=".$productimage["url"]."/>";
// }
// }
}
