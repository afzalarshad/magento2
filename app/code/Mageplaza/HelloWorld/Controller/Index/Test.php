<?php
namespace Mageplaza\HelloWorld\Controller\Index;

use Magento\Framework\View\Result\PageFactory;

class Test implements \Magento\Framework\App\ActionInterface
{
	protected PageFactory $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
	}

	public function execute()
	{
		echo "Hello World";
		exit;
	}
}
