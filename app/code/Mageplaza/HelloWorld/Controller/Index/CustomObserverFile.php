<?php
namespace Mageplaza\HelloWorld\Controller\Index;

class CustomObserverFile extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;
    

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $pageFactory = $this->_pageFactory->create();
        $this->_eventManager->dispatch('md_customobserver_log',['custom_text' => 'Custom Observer']);
        $pageFactory->getConfig()->getTitle()->prepend(__('Welcome to Magento Observer Module'));
        return pageFactory;
    }
}
