<?php
namespace Mageplaza\HelloWorld\Observer;
use Psr\Log\LoggerInterface;

class CustomObserver implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct()
    {
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // $myEventData = $observer->getData('myEventData'); 
        $observer_data = $observer->getData('custom_text');
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/custom.log');
        $logger = new \Zend_Log();
        $logger -> addWritter($writer);
        $logger -> info($observer_data);

        return $this;
    }
}