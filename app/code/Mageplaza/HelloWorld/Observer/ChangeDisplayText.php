<?php
namespace Mageplaza\HelloWorld\Observer;

class ChangeDisplayText implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct()
    {
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // $myEventData = $observer->getData('myEventData');
        $displayText = $observer->getData('mp_text');
        echo $displayText->getText() . "- Event </br>";
        $displayText->setText('Execute event successfully.');

        return $this;
    }
}
