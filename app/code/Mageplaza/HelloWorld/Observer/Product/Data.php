<?php
namespace Mageplaza\HelloWorld\Observer\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Data implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct()
    {
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // $myEventData = $observer->getData('myEventData');
        $product = $observer->getProduct();
        $originalName = $product->getName();
        $modifiedName = $originalName . ' Modified by Magento 2 Events and Observers';
        $product->setName($modifiedName);
    }
}