<?php
namespace Vendor\Extension\Block\Adminhtml\Edit;

class Save extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
    public function getButtonData()
 
    {
 
        return [
 
            'label' => __('Save'),
 
            'class' => 'save primary',
 
            'data_attribute' => [
 
                'mage-init' => ['button' => ['event' => 'save']],
 
                'form-role' => 'save',
 
            ],
 
            'sort_order' => 90,
 
        ];
 
    }
}
