<?php
namespace Vendor\Module\Block;
use Magento\Framework\App\Bootstrap;


class Customer extends \Magento\Framework\View\Element\Template
{
    private \Magento\Customer\Model\CustomerFactory $customerFactory;
    private \Magento\Store\Model\StoreManagerInterface $storeManager;

    public function __construct(
 \Magento\Framework\View\Element\Template\Context $context,
 \Magento\Store\Model\StoreManagerInterface $storeManager,
 \Magento\Customer\Model\CustomerFactory $customerFactory,
 array $data = []
 ) {
 $this->storeManager = $storeManager;
 $this->customerFactory = $customerFactory;
 parent::__construct($context, $data);
 }

    /** Create customer
     * Pass customer data as array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
 public function createCustomer($data): void
 {
$store = $this->storeManager->getStore();
 $storeId = $store->getStoreId();
 $websiteId = $this->storeManager->getStore()->getWebsiteId();
 $customer = $this->customerFactory->create();
 $customer->setWebsiteId($websiteId);
 $customer->loadByEmail($data['customer']['email']);// load customer by email address
 if(!$customer->getId()){
 //For guest customer create new cusotmer
 $customer->setWebsiteId($websiteId)
 ->setStore($store)
 ->setFirstname($data['customer']['firstname'])
 ->setLastname($data['customer']['lastname'])
 ->setPrefix($data['customer']['prefix'])
 ->setEmail($data['customer']['email'])
 ->setPassword($data['customer']['password']);
 $customer->save();
 }
 }
}
