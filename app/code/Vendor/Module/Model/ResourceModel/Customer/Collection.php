<?php
namespace Vendor\Module\Model\ResourceModel\Customer;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'vendor_module_customer_collection';
    protected $_eventObject = 'customer_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vendor\Module\Model\Customer', 'Vendor\Module\Model\ResourceModel\Customer');
    }
}
