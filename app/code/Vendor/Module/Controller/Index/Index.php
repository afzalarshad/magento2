<?php
namespace Vendor\Module\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;
    protected $storeManager;
    protected $customerFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
       \Magento\Store\Model\StoreManagerInterface $storeManager,
       \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        // $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // $customer   = $this->customerFactory->create();
        // $customer->setWebsiteId($websiteId);
        // $customer->setEmail("john1@example.com"); 
        // $customer->setFirstname("John");
        // $customer->setLastname("Dee");
        // $customer->setPassword("123XYZ");
        // $customer->save();
        // $customer->sendNewAccountEmail();
        // return $this->_pageFactory->create();
        echo "Hello World!";
        // exit;
// ===============================================================

        // $customer   = $this->customerFactory->create();
		// $collection = $customer->getCollection();
		// $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // $customer->setWebsiteId($websiteId);
        // foreach($collection as $item){
        //     	// $customer->set$item->getData());
        //         $customer->setFirstname($item['firstname']);
        //         $customer->setEmail($item['email']);
        //         $customer->setLastname($item['lastname']);
        //     }
        // $customer->setFirstname('firstname');
        // $customer->setEmail("john1@example.com"); 
        // $customer->setLastname("Dee");
        // $customer->setPassword("123XYZ");
        // $customer->save();
		// exit();
// ===============================================================
        // $post = $this->_postFactory->create();
		// $collection = $post->getCollection();
		// foreach($collection as $item){
		// 	echo "<pre>";
		// 	print_r($item->getData());
		// 	echo "</pre>";
		// }
		// exit();
		return $this->_pageFactory->create();
    }
}
